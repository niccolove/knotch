/*
    SPDX-FileCopyrightText: 2013 Eike Hein <hein@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick 2.0
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.0

import org.kde.kirigami 2.19 as Kirigami
import org.kde.plasma.core 2.0 as PlasmaCore

Kirigami.FormLayout {
    width: childrenRect.width
    height: childrenRect.height

    property alias cfg_notchWidth: notchWidth.value
    property alias cfg_notchRadius: notchRadius.value
    property alias cfg_notchMargin: notchMargin.value

    QQC2.Slider {
        id: notchWidth
        Kirigami.FormData.label: i18nc("@title:group", "Notch Width:")
        from: 5
        to: 300
        stepSize: 5
    }

    RowLayout {
       Layout.fillWidth: true

        QQC2.Label {
            text: i18nc("@item:inrange", "Smally")
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Label {
            text: i18nc("@item:inrange", "Biggy")
        }
    }

    QQC2.Slider {
        id: notchRadius
        Kirigami.FormData.label: i18nc("@title:group", "Notch Radius:")
        from: 0
        to: 50
        stepSize: 1
    }

    RowLayout {
       Layout.fillWidth: true

        QQC2.Label {
            text: i18nc("@item:inrange", "Kinda None")
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Label {
            text: i18nc("@item:inrange", "A lot")
        }
    }

    QQC2.Slider {
        id: notchMargin
        Kirigami.FormData.label: i18nc("@title:group", "Notch Margin:")
        from: 0
        to: 30
        stepSize: 1
    }

    RowLayout {
       Layout.fillWidth: true

        QQC2.Label {
            text: i18nc("@item:inrange", "Kinda none")
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Label {
            text: i18nc("@item:inrange", "So much")
        }
    }

    QQC2.Slider {
        Kirigami.FormData.label: i18nc("@title:group", "Notch Black Shade:")
        from: 0
        to: 30
        stepSize: 3
    }

    RowLayout {
       Layout.fillWidth: true

        QQC2.Label {
            text: i18nc("@item:inrange", "Very Black")
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Label {
            text: i18nc("@item:inrange", "Extremely Black")
        }
    }

    QQC2.Slider {
        Kirigami.FormData.label: i18nc("@title:group", "N. Of Sensors Under Notch:")
        from: 0
        to: 5
        stepSize: 1
    }

    RowLayout {
       Layout.fillWidth: true

        QQC2.Label {
            text: i18nc("@item:inrange", "A few")
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Label {
            text: i18nc("@item:inrange", "A bunch")
        }
    }

    QQC2.Slider {
        Kirigami.FormData.label: i18nc("@title:group", "Notchness:")
        from: 0
        to: 2
        stepSize: 1
    }

    RowLayout {
       Layout.fillWidth: true

        QQC2.Label {
            text: i18nc("@item:inrange", "Not Notch-y")
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Label {
            text: i18nc("@item:inrange", "Notch-y")
        }
    }

    QQC2.Slider {
        Kirigami.FormData.label: i18nc("@title:group", "Money Donated To KDE:")
        from: 0
        to: 1000
        stepSize: 1
    }

    RowLayout {
       Layout.fillWidth: true

        QQC2.Label {
            text: i18nc("@item:inrange", "None Yet")
        }

        Item {
            Layout.fillWidth: true
        }

        QQC2.Label {
            text: i18nc("@item:inrange", "A Lot")
        }
    }
}
